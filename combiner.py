#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import io

def UnicodeSanitize( dictItem ):
    print( "\n" )
    newItem = {}
    for key, value in dictItem.iteritems():
        newItem[key] = value
        print( key.decode( "utf-8" ), value.decode( "utf-8" ) )

    return newItem

# https://stackoverflow.com/questions/21572175/convert-csv-file-to-list-of-dictionaries
def ReadCsv( filename ):
    csvFile = open( filename, "r" )

    data = []
    fields = []
    line = 0

    for row in csv.DictReader( csvFile, skipinitialspace=True ):
        data.append( row )
        
    return data

def SaveToFile( filename, dataList ):
    phraseFile = open( filename, "w" )
    phraseFile.write( "laadan,english\n" )

    for item in dataList:
        phraseFile.write( item["laadan"] + "," + item["english"] + "\n" )

entries = []

pronouns = ReadCsv( "dictionaries/laadan-pronouns.csv" )
intransitiveVerbs = ReadCsv( "dictionaries/laadan-intransitive-verbs.csv" )

for p in range( len( pronouns ) ):
    for v in range( len( intransitiveVerbs ) ):
        newEntry = {}

        fixedVerb = intransitiveVerbs[v]["english"]

        if ( fixedVerb.find( "to be" ) != -1 ):
            fixedVerb = fixedVerb.replace( "to be", "is" )

        elif ( fixedVerb.find( "to" ) != -1 ):
            fixedVerb = fixedVerb.replace( "to", "" )
            fixedVerb += "s"
            
        newEntry["english"] = pronouns[p]["english"] + " " + intransitiveVerbs[v]["english present tense"]
        newEntry["laadan"] = intransitiveVerbs[v]["láadan"] + " " + pronouns[p]["láadan"]
        entries.append( newEntry )

SaveToFile( "phrases1.csv", entries )


